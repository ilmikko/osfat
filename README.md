# osfat

File allocation tables implemented for Operating Systems

The shell executable takes arguments a, b, c, or d to run a respective part of the assessment.

For example, `./shell B` runs until the B3 - B1 part of the assessment.

## Building and running

In order to build and run all of the steps, use `make run`.

You can alternatively only build the code using `make build`.

Please refer to `shell.c` for the high-level overview of the file creation process.

## Source

You can inspect the source code for each part in `lib/gcs_x.c`, where `x` is the letter of the part.

Please note that the previous parts have been updated by convenience methods given by the later parts.

This means that sourcing only one file will not work in all cases; this is intentional,
as I want to keep the methods defined in the parts where they were first needed.
