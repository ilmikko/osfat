#include <stdio.h>
#include <string.h>
#include "lib/filesys.h"
#define TEXT_SIZE 4096

void listfiles(char ** list) {
	for (int i = 0; list[i] != NULL; i++) {
		printf("%s\n", list[i]);
	}
}

// GCS D mark.
void gcs_d() {
	format();
	writedisk("virtualdiskD3_D1");
}

// GCS C mark.
void gcs_c() {
	{
		MyFILE * file = myfopen("/testfile.txt", "w");
		char text[TEXT_SIZE];
		generatestuff(text, TEXT_SIZE);

		for (int i = 0; i < TEXT_SIZE; i++) {
			myfputc(text[i], file);
		}

		myfclose(file);
	}

	writedisk("virtualdiskC3_C1");

	{
		MyFILE * file = myfopen("/testfile.txt", "r");
		for (int i = 0;; i++) {
			char c = myfgetc(file);
			if (c == EOF) break;
			printf("%c", c);
		}
		printf("\n");
		// myfclose(file);
	}
}

// GCS B mark.
void gcs_b() {
	// Working like unix mkdir, path needs to exist first.
	mymkdir("/myfirstdir");
	mymkdir("/myfirstdir/myseconddir");
	mymkdir("/myfirstdir/myseconddir/mythirddir");

	{
		char ** list = mylistdir("/myfirstdir/myseconddir");
		listfiles(list);
	}
	writedisk("virtualdiskB3_B1_a");

	MyFILE * file = myfopen("/myfirstdir/myseconddir/testfile.txt", "w");
	myfclose(file);

	{
		char ** list = mylistdir("/myfirstdir/myseconddir");
		listfiles(list);
	}
	writedisk("virtualdiskB3_B1_b");
}

void gcs_a() {
	mymkdir("/firstdir");
	mymkdir("/firstdir/seconddir");
	{
		MyFILE * tf = myfopen("/firstdir/seconddir/testfile1.txt", "w");
		const char * str = "Hello there!";
		for (int i = 0; i < strlen(str); i++) {
			myfputc(str[i], tf);
		}
		myfclose(tf);
	}
	{
		char ** list = mylistdir("/firstdir/seconddir");
		listfiles(list);
	}

	mychdir("/firstdir/seconddir");
	{
		char ** list = mylistdir(".");
		listfiles(list);
	}

	{
		MyFILE * tf = myfopen("testfile2.txt", "w");
		const char * str = "Ah ..! What's happening?\nEr, excuse me, who am I?\nHello?\nWhy am I here? What's my purpose in life?\nWhat do I mean by who am I?";
		for (int i = 0; i < strlen(str); i++) {
			myfputc(str[i], tf);
		}
		myfclose(tf);
	}

	mymkdir("thirddir");
	{
		MyFILE * tf = myfopen("thirddir/testfile3.txt", "w");
		const char * str = "Test 3!";
		for (int i = 0; i < strlen(str); i++) {
			myfputc(str[i], tf);
		}
		myfclose(tf);
	}

	writedisk("virtualdiskA5_A1_a");
}

int main(int argc, char ** argv) {
	if (argc < 2) {
		printf("Usage: %s [A|B|C|D]\nThis will run the different parts of the assessment.\n", argv[0]);
		return 1;
	} else {
		if (!strcmp(argv[1], "D") || !strcmp(argv[1], "d")) {
			gcs_d();
		} else if (!strcmp(argv[1], "C") || !strcmp(argv[1], "c")) {
			gcs_d();
			gcs_c();
		} else if (!strcmp(argv[1], "B") || !strcmp(argv[1], "b")) {
			gcs_d();
			gcs_c();
			gcs_b();
		} else if (!strcmp(argv[1], "A") || !strcmp(argv[1], "a")) {
			gcs_d();
			gcs_c();
			gcs_b();
			gcs_a();
		}
	}
	return 0;
}
