all: build

build:
	gcc -o shell lib/filesys.c shell.c;

clean:
	rm vgcore.*
	rm virtualdisk*

run: build
	./shell a
	hexdump -C virtualdiskA5_A1_a

trace: build
	./shell d > traceD3_D1.txt
	./shell c > traceC3_C1.txt
	./shell b > traceB3_B1.txt
	./shell a > traceA5_A1.txt
