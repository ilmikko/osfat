/**
 * gcs_b.c
 * Source code for the GCS B3-B1 parts of the assessment.
 * */

char * dirname(const char * path) {
	char * dirpath = malloc(strlen(path) + 1);

	int capture = 0;
	for (int i = strlen(path)-1; i >= 0; i--){
		if (capture) {
			dirpath[i] = path[i];
		} else if (path[i] == '/') { 
			capture = 1;
			dirpath[i] = '\0';
		} else {
			dirpath[i] = '\0';
		}
	}

	return dirpath;
}

char * basename(const char * path) {
	char * basepath = malloc(strlen(path) + 1);

	for (int i = 0; i < strlen(path) + 1; i++) basepath[i] = '\0';

	for (int i = 0, j = 0; i < strlen(path) + 1; i++, j++) {
		if (path[i] == '/') { 
			j = -1;
		} else {
			basepath[j] = path[i];
		}
	}

	return basepath;
}

diskblock_t * seek_dir(const char * path) {
	char * token = "",
			 * ctx = "",
			 * string = "";

	// Determine whether to seek from root or current directory.
	if (path[0] != '/') {
		string = malloc(strlen(currentDir)+1+strlen(path)+1);

		strcpy(string, currentDir);
		strcat(string, "/");
		strcat(string, path);
	} else {
		string = malloc(strlen(path)+1);

		strcpy(string, path);
	}

	// As currentDir always starts from root, we are ensured here that we always work on root paths.
	// This means that . and .. become simple string manipulations we can do using normalize.

	diskblock_t * current_block = root_dir;

	char * current_path = normalize(string);

	token = strtok_r(current_path, "/", &ctx);

	while (token) {
		direntry_t * next_entry = find_direntry(&current_block->dir, token);

		if (!next_entry) {
			printf("[1mI have failed, %s not found[m\n", token);
			return NULL;
		}

		// Check if it's a directory.
		if (!next_entry->isdir) {
			printf("[1mI have failed, %s is not a directory[m\n", token);
			return NULL;
		}

		// All is good!
		current_block = readblock(next_entry->firstblock);

		token = strtok_r(NULL, "/", &ctx);

		if (!token) break;
	}

	return current_block;
}

direntry_t newdir(const char * name) {
	direntry_t entry;
	entry.isdir = 1;

	// Why do I need these? fml
	entry.entrylength = 0;
	entry.unused = 0;
	entry.modtime = 0;
	entry.filelength = 0;

	int clearing = 0;
	for (int i = 0; i < MAXNAME; i++) {
		if (clearing) {
			entry.name[i] = '\0';
		} else {
			entry.name[i] = name[i];
			if (name[i] == '\0') {
				entry.entrylength = i;
				clearing = 1;
			}
		}
	}

	return entry;
}

void mymkdir(const char * path) {
	char * string = malloc(strlen(path) + 1);
	strcpy(string, path);

	char * _dirname = dirname(path);
	char * _basename = basename(path);

	diskblock_t * dir_block = seek_dir(_dirname);
	if (!dir_block) {
		printf("[1mDirectory %s was not found[m", _dirname);
		return;
	}

	direntry_t entry = newdir(_basename);

	entry.firstblock = next_free_block();

	write_direntry(entry, dir_block);

	free(string);
}

char ** mylistdir(char * path) {
	printf("List dir: %s\n", path);
	diskblock_t * dir_block = seek_dir(path);
	if (!dir_block) {
		printf("[1mPath not found: %s[m\n", path);
		return NULL;
	}

	int arr_size = dir_block->dir.nextEntry;
	char ** test = malloc((arr_size+1) * sizeof(char *));
	for (int i = 0; i < arr_size; i++) {
		test[i] = dir_block->dir.entrylist[i].name;
	}
	test[arr_size] = NULL;
	return test;
}
