/**
 * gcs_c.c
 * Source code for the GCS C3-C1 parts of the assessment.
 * */

// TODO: change ints to fatentry (short)
int next_free_block() {
	for (int i = 0; i < MAXBLOCKS; i++) {
		int addr = read_fat_table(i);
		if (addr == UNUSED) {
			printf("Initiated BLOCK %i\n", i);
			// Allocate the block in the fat table
			write_fat_table(ENDOFCHAIN, i);
			return i;
		}
	}
	printf("[1mBailing out; there are no free blocks left.[m\n");
}

void resetbuffer(MyFILE * stream) {
	stream->pos = 0;
	diskblock_t buf;
	stream->buffer = buf;
}

void writebuffer(MyFILE * stream) {
	// Write block.
	int blockno = stream->blockno;
	diskblock_t * disk_block = readblock(blockno);
	for (int i = 0; i < stream->pos; i++) {
		disk_block->data[i] = stream->buffer.data[i];
	}
	writeblock(disk_block, blockno);
}

// Returns NULL if block is not found.
direntry_t * find_direntry(dirblock_t * dir, const char * name) {
	for (int i = 0; i < dir->nextEntry; i++) {
		direntry_t * entry = &dir->entrylist[i];
		if (!strcmp(entry->name, name)) {
			// Found!
			return entry;
		}
	}
	return NULL;
}

void write_direntry(direntry_t entry, diskblock_t * dirblock) {
	dirblock->dir.entrylist[dirblock->dir.nextEntry++] = entry;
}

MyFILE * myfopen(const char * path, const char * mode) {
	MyFILE * fd = malloc(sizeof(MyFILE));

	// TODO: sizeof(MyFILE->mode)
	for (int i = 0; i < 3; i++) {
		fd->mode[i] = mode[i];
	}

	resetbuffer(fd);

	// Find the directory node that we are writing into.
	char * _dirname = dirname(path);
	char * _basename = basename(path);

	diskblock_t * dir_block = seek_dir(_dirname);
	if (!dir_block) {
		printf("Cannot find dir %s!\n", _dirname);
		return NULL;
	}

	if (!strcmp(fd->mode, "w")) {
		// Write the directory entry.
		direntry_t entry;
		entry.isdir = 0;
		fd->blockno = next_free_block();
		entry.firstblock = fd->blockno;

		// TODO: Any way to implicitly zero these? I thought `direntry_t entry` was supposed to do that.
		entry.entrylength = 0;
		entry.unused = 0;
		entry.modtime = 0;
		entry.filelength = 0;

		int clearing = 0;

		// TODO: strcpy?
		for (int i = 0; i < MAXNAME; i++) {
			if (clearing) {
				entry.name[i] = '\0';
			} else {
				entry.name[i] = _basename[i];
				if (_basename[i] == '\0') {
					entry.entrylength = i;
					clearing = 1;
				}
			}
		}

		write_direntry(entry, dir_block);
	}

	if (!strcmp(fd->mode, "r")) {
		// TODO: Can we do better than O(n)?
		// Find a directory entry.
		direntry_t * entry = find_direntry(&dir_block->dir, _basename);
		if (entry) {
			fd->blockno = entry->firstblock;
			fd->buffer = *readblock(entry->firstblock);
		} else {
			printf("[1mFailed to open file for reading; does not exist.[m\n");
		}
	}

	return fd;
}

void myfclose(MyFILE * stream) {
	if (!strcmp(stream->mode, "w")) {
		myfputc(EOF, stream);
		writebuffer(stream);
	}
	free(stream);
}

void myfputc(int b, MyFILE * stream) {
	if (strcmp(stream->mode, "w")) { return; }

	stream->buffer.data[stream->pos] = b;
	stream->pos++;
	if (stream->pos == BLOCKSIZE) {
		writebuffer(stream);
		int old_block = stream->blockno;
		resetbuffer(stream);
		stream->blockno = next_free_block();
		// Update FAT.
		write_fat_table(stream->blockno, old_block);
	}
}

int myfgetc(MyFILE * stream) {
	char chr = stream->buffer.data[stream->pos];
	stream->pos++;
	if (stream->pos == BLOCKSIZE) {
		int next_block = read_fat_table(stream->blockno);
		resetbuffer(stream);
		stream->blockno = next_block;
		stream->buffer = *readblock(next_block);
	}
	return chr;
}

void generatestuff(char * buf, int size) {
	static char * whales = "Whales";
	for (int i = 0; i < size - 1; i++) { buf[i] = whales[i%6]; }
	buf[size - 1] = 0;
}
