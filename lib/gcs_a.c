/**
 * gcs_a.c
 * Source code for the GCS A5-A1 parts of the assessment.
 * */

char * normalize(char * path) {
	int len = strlen(path);
	char * new_path = malloc(len+1);

	for (int i = 0, skip = 0; i < len + 1; i++) {
		if (i > 0 && new_path[i-skip-1] == '/' && path[i] == '/') {
			skip++;
		}
		new_path[i-skip] = path[i];
	}

	char * ctx, * token;

	char * normalized_path = malloc(len+1);

	token = strtok_r(new_path, "/", &ctx);
	while(token) {
		if (!strcmp(token, "..")) {
			normalized_path = dirname(normalized_path);
		} else if (strcmp(token, ".")) {
			strcat(normalized_path, "/");
			strcat(normalized_path, token);
		}
		token = strtok_r(NULL, "/", &ctx);

		if (!token) break;
	}

	return normalized_path;
}

void mychdir(char * path) {
	char * new_path = malloc(strlen(currentDir)+1+strlen(path)+1);

	strcpy(new_path, currentDir);
	strcat(new_path, "/");
	strcat(new_path, path);

	char * normalized = normalize(new_path);

	free(new_path);

	printf("Chdir from %s to %s\n", currentDir, normalized);
	currentDir = normalized;
}

void myremove(char * path) {

}

void myrmdir(char * path) {

}
