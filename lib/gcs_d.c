/**
 * gcs_d.c
 * Source code for the GCS D3-D1 parts of the assessment.
 * */

#define FAT_INDEX 1

// TODO: rename to "formatblock" or something
diskblock_t * newblock(char init_char) {
	diskblock_t * block = malloc(sizeof(diskblock_t));
	for (int i = 0; i < BLOCKSIZE; i++) {
		block->data[i] = init_char;
	}
	return block;
}

diskblock_t * fat_table, * root_dir;

int read_fat_table(int index) {
	return fat_table->fat[index];
}

void write_fat_table(int data, int index) {
	fat_table->fat[index] = data;
	writeblock(fat_table, FAT_INDEX);
}

void format() {
	int current_block = 0;
	int fatentry = 0;

	/* prepare block 0 : fill it with '\0',
	 * use strcpy() to copy some text to it for test purposes
	 * write block 0 to virtual disk
	 */
	{
		diskblock_t * block = newblock(0);
		strcpy(block->data, "CS3026 Operating Systems Assessment");
		writeblock(block, current_block++);
		free(block);
	}

	/* prepare FAT table
	 * write FAT blocks to virtual disk
	 */
	{
		int fatblocksneeded = (MAXBLOCKS / FATENTRYCOUNT);
		for (int b = 0; b < fatblocksneeded; b++) {
			diskblock_t * block = newblock(UNUSED);
			writeblock(block, current_block++);
			free(block);
		}
		// Occupy the fat table on the fat table itself.
		fat_table = readblock(FAT_INDEX);
		write_fat_table(ENDOFCHAIN, 0);
		for (int i = 1; i <= fatblocksneeded; i++) {
			if (i == fatblocksneeded) {
				write_fat_table(ENDOFCHAIN, i);
			} else {
				write_fat_table(i+1, i);
			}
		}
	}

	/* prepare root directory
	 * write root directory block to virtual disk
	 */
	{
		rootDirIndex = current_block;

		// Occupy the fat entry.
		write_fat_table(ENDOFCHAIN, rootDirIndex);

		// Write the root directory.
		root_dir = readblock(rootDirIndex);
		dirblock_t * dir = &root_dir->dir;

		dir->isdir = 1;
		dir->nextEntry = 0;

		writeblock(root_dir, rootDirIndex);
	}
}
